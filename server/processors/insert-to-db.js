/**
 * Created by User on 28/08/2016.
 */

var BaseStep = require('@guggy/guggy-js-utils').StepRunner.BaseStep;
var Consts = require(`../../consts/consts.js`);
var Mongo = require('@guggy/guggy-js-utils').MongoDb.init({url: Consts.MongoDataDb.url});
var Collections = Consts.MongoDataDb.collections;

class InsertToDB extends BaseStep {

    run() {

        var data = this.opts.data;

        var criteria = {_id: data.result.id};

        delete data.result.id;
        var action = {$set: data.result};

        return Mongo.collection(Collections.ContentStudio).updateP(criteria, action, {upsert: true, multi: true}).then(() => {
            var sourceAction = {$setOnInsert: {type: "youtube"}};
            return Mongo.collection(Collections.ContentStudioSources).updateP(criteria, sourceAction, {upsert: true})
        }).then(() => {

            return true;

        });

    }

}

module.exports = InsertToDB;
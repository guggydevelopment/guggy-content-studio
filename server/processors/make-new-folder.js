/**
 * Created by User on 28/08/2016.
 */

/**
 * Created by shahar on 6/19/2016.
 */

var _ = require('lodash');
var BaseStep = require('@guggy/guggy-js-utils').StepRunner.BaseStep;
var fs = require('fs-extra');
var Q = require('q');


class MakeNewFolder extends BaseStep {

    run() {

        var data = this.opts.data;
        return Q.nfcall(fs.mkdirp, data.mediaPath);

    }


}

module.exports = MakeNewFolder;
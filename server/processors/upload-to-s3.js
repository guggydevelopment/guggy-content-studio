/**
 * Created by User on 28/08/2016.
 */

var BaseStep = require('@guggy/guggy-js-utils').StepRunner.BaseStep;
var S3 = require('@guggy/guggy-js-utils').S3;
var ContentStudioAws = require('../../consts/consts.js').ContentStudioAWS;
var fs = require('fs-extra');
var path = require('path');
var _ = require('lodash');
var Consts = require(`../../consts/consts.js`);
var Mongo = require('@guggy/guggy-js-utils').MongoDb.init({url: Consts.MongoDataDb.url});
var Collections = Consts.MongoDataDb.collections;
var Promise = require('bluebird');
var async = require('async');

class UploadToS3AndSave extends BaseStep {

    run() {

        var data = this.opts.data;

        var id = data.result.id;
        delete data.result.id;

        data.uploadToS3 = _.orderBy(data.uploadToS3, ['index'], ['asc']);

        return Promise.mapSeries(data.uploadToS3, (item, index) => {

            var mediaBody = fs.createReadStream(path.join(data.mediaPath, item.fileName));

            return S3.upload(mediaBody, item.fileName, ContentStudioAws, () => {
                console.log('retry')
            }).then(result => {

                result = {fileName: result, type: 'mp4'}; // todo handle type if has changes

                fs.remove(path.join(data.mediaPath, result.fileName));

                let rawUrl = `${ContentStudioAws.url}/${result.fileName}`;
                let url = _.replace(rawUrl, ContentStudioAws.url, ContentStudioAws.cdnUrl);
                url = _.replace(url, 'http', 'https');

                var criteria = {_id: id};

                var action = {$setOnInsert: data.result, $addToSet: {'urls': url}};

                return Mongo.collection(Collections.ContentStudio).updateP(criteria, action, {
                    upsert: true,
                    multi: true
                }).then(() => {

                    ++data.process.lastPartUploaded;

                });


            });

        });
    }

}

module.exports = UploadToS3AndSave;
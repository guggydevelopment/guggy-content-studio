/**
 * Created by User on 28/08/2016.
 */

/**
 * Created by shahar on 6/19/2016.
 */

var _ = require('lodash');
var BaseStep = require('@guggy/guggy-js-utils').StepRunner.BaseStep;
var YoutubeUtils = require('../../../utils/youtube-utils.js');
var ConstsContentStudio = require('../../../consts/consts.js').ContentStudio;

class DownloadYoutubeVideo extends BaseStep {

    run() {

        var data = this.opts.data;

        return YoutubeUtils.download(data.youtubeUrl, data.result.id, data.mediaPath).then(result => {

            if(result && result[0]) {
                let persistResult = JSON.parse(result[0]);
                data.duration = persistResult.duration;
                data.result.height = parseInt((ConstsContentStudio.splitVideoWidth / (persistResult.width / persistResult.height) / 2) * 2); // (ow / aspectRatio / 2) *2
                data.result.width = parseInt(ConstsContentStudio.splitVideoWidth);
            }

        });
    }


}

module.exports = DownloadYoutubeVideo;
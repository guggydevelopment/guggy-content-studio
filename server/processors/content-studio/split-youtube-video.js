/**
 * Created by User on 28/08/2016.
 */

/**
 * Created by shahar on 6/19/2016.
 */

var _ = require('lodash');
var BaseStep = require('@guggy/guggy-js-utils').StepRunner.BaseStep;
var fs = require('fs-extra');
var path = require('path');
var Q = require('q');
var YoutubeUtils = require('../../../utils/youtube-utils.js');
var Promise = require('bluebird');
var Consts = require('../../../consts/consts.js');
var ConstsContentStudio = Consts.ContentStudio;
var Mongo = require('@guggy/guggy-js-utils').MongoDb.init({url: Consts.MongoDataDb.url});
var Collections = Consts.MongoDataDb.collections;
var StatusSplitVideoEnum = require('../../../enums/status-split-video.js');
var async = require('async');

class SplitYoutubeVideo extends BaseStep {

    run() {

        var data = this.opts.data;

        var originalVideoMediaPath = path.join(data.mediaPath, data.result.id);

        var increment = ConstsContentStudio.incrementSplitVideo;

        var promises = [];

        data.uploadToS3 = [];

        var lastPartUploaded = -1;
        var numbersOfUploads = 0;

        if(data.process && data.process.lastPartUploaded ) {

            lastPartUploaded = data.process.lastPartUploaded;

        }

        data.process.lastPartUploaded = lastPartUploaded === -1 ? 0 : lastPartUploaded;
        var i = data.process.lastPartUploaded * increment;
        var indexFileName = data.process.lastPartUploaded + 1;

        while(i < data.duration && indexFileName > lastPartUploaded && numbersOfUploads < ConstsContentStudio.uploadsPerBatch) {

            promises.push({
                i: i,
                indexFileName: indexFileName
            });

            ++lastPartUploaded;
            ++numbersOfUploads;
            ++indexFileName;
            i += increment;

        }

        if(i >= data.duration) {

            data.process.isLastBatch = true;

        }

        return Promise.map(promises, (item, index) => {

            return YoutubeUtils.splitVideos(item.i, originalVideoMediaPath, data.mediaPath, item.indexFileName, data.result.id, ConstsContentStudio.splitVideoWidth).then(() => {

                return data.uploadToS3.push({
                    fileName: `${data.result.id}${data.process.lastPartUploaded + 1 + index}.mp4`,
                    type: 'mp4',
                    index: index
                });


            });

        }, {concurrency: 5});

    }
}

module.exports = SplitYoutubeVideo;

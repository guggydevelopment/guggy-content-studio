/**
 * Created by User on 28/08/2016.
 */

var _ = require('lodash');
var Consts = require('../../consts/consts.js');
var MongoDataDb = require('@guggy/guggy-js-utils').MongoDb.init({url: Consts.MongoDataDb.url});
var CollectionsDataDB = Consts.MongoDataDb.collections;
var BaseStep = require('@guggy/guggy-js-utils').StepRunner.BaseStep;

class FetchFromGuggyMedia extends BaseStep {

    run() {

        var data = this.opts.data;
        return MongoDataDb.collection(CollectionsDataDB.ContentStudio).findOneP({"_id":data.result.id}).then(result => {

            if (result) {

                data.result = result;
                data.result.isVideosInQueue = true;
                data.isFinished = true;

            } else {

                return MongoDataDb.collection(CollectionsDataDB.ContentStudioSources).findOneP({"_id":data.result.id});

            }

        }).then(result => {

            if (result) {

                data.result.isVideosUploadedBefore = true;
                data.isFinished = true;

            }

        });

    }

}
module.exports = FetchFromGuggyMedia;
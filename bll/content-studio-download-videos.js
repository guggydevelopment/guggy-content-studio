/**
 * Created by User on 25/08/2016.
 // */
var path = require('path');
var _ = require('lodash');
var fs = require('fs-extra');
var StepRunner = require('@guggy/guggy-js-utils').StepRunner.StepRunner;
var MakeNewFolder = require('../server/processors/make-new-folder.js');
var FetchFromMedia = require('../server/processors/fetch-from-guggy-media.js');
var DownloadYoutubeVideo = require('../server/processors/content-studio/download-youtube-video.js');
var SplitYoutubeVideo = require('../server/processors/content-studio/split-youtube-video.js');
var insertToDB = require('../server/processors/insert-to-db.js');
var UploadToS3AndSave = require('../server/processors/upload-to-s3.js');
var Utils = require('../utils/utils.js');
var Consts = require(`../consts/consts.js`);
var Mongo = require('@guggy/guggy-js-utils').MongoDb.init({url: Consts.MongoDataDb.url});
var Collections = Consts.MongoDataDb.collections;
var StatusSplitVideoEnum = require('../enums/status-split-video.js');
var UpdateProcessingMode = require('../server/processors/update-processing-mode.js')

var splitFramesObjSteps = [

    {step: FetchFromMedia, shouldRunFunc: data => !data.isInProcess && !fs.existsSync(data.mediaPath)},
    {step: UpdateProcessingMode, shouldRunFunc: data => !data.isInProcess},
    {step: MakeNewFolder, shouldRunFunc: data => !fs.existsSync(data.mediaPath)},
    {step: DownloadYoutubeVideo, shouldRunFunc: data => !fs.existsSync(`${data.mediaPath}/${data.result.id}.mp4`)},
    {step: SplitYoutubeVideo},
    {step: UploadToS3AndSave}

];

function processRawData(rawData, data) {

    rawData.isInProcess = true;
    rawData.process = {};
    rawData.process.lastPartUploaded = data.process.lastPartUploaded;
    rawData.videoInfo = {};
    rawData.videoInfo.duration = data.duration;
    rawData.videoInfo.height = data.result.height;
    rawData.videoInfo.width = data.result.width;

    return rawData;


}

var contentStudio = function (rawData, callback) {

    var data = {};
    data.isInProcess = rawData.isInProcess;
    data.folderName = Consts.ContentStudio.FOLDER_NAME;
    data.subFolderName = Consts.ContentStudio.SUB_FOLDER_NAME;
    data.mediaPath = path.join(global.appRoot, data.folderName, data.subFolderName);
    data.contentStudio = true;

    data.youtubeUrl =  rawData.url;
    data.result = {};

    if(rawData.videoInfo) {

        data.duration = rawData.videoInfo.duration;
        data.result.height = rawData.videoInfo.height; // (ow / aspectRatio / 2) *2
        data.result.width = rawData.videoInfo.width; // (ow / aspectRatio / 2) *2

    }
    data.process = rawData.process ? rawData.process : {};
    data.process.lastPartUploaded = data.process.lastPartUploaded ? data.process.lastPartUploaded : rawData.lastPartUploaded;

    data.result.duration = Consts.ContentStudio.durationSplitVideo;
    var params = Utils.searchQueryToJSON(data.youtubeUrl);

    var id;
    if(params && params.v) {

        id = params.v;

    }

    data.result.id = id;

    return new StepRunner({
        name: 'ContentStudioDownloadVideos',
        data: data,
        steps: splitFramesObjSteps,
        debug: true,
        didEndFunc: data => data.isFinished
    }).run().then(data => {

        if(data && data.result) {

            if(data.isFinished) data.isFinished = false;
            return data.result;

        }

    }).catch(err => {

        let failsCount;

        return Mongo.collection(Collections.ContentStudioWaitingList).findOneP({"url":data.youtubeUrl}).then(result => {

            failsCount = result && result.failsCount ? result.failsCount + 1 : 1;

            if(failsCount >= Consts.ContentStudio.maxFailsCount) {

                return false;

            } else {

                return Mongo.collection(Collections.ContentStudioWaitingList).updateP({"url":rawData.url}, {$set: {"failsCount": failsCount}});

            }

        }).then(result => {

            if(!result) {

                return Mongo.collection(Collections.ContentStudioWaitingList).updateP({"url": data.youtubeUrl}, {$set: {"status": StatusSplitVideoEnum.Types.FAIL}}).then(() => {

                    data.error = err;

                });

            }

        });

    }).finally(() => {

        //clear the folder
        return Mongo.collection(Collections.ContentStudioWaitingList).updateP({"url": data.youtubeUrl}, {$set: {lastPartUploaded : data.process.lastPartUploaded}}, {upsert: true}).then(() => {

            if(data.error) {

                console.log(`${data.youtubeUrl} failed`);
                callback(data.error);
                return false;

            } else if (data.process.isLastBatch || data.isFinished) {

                //todo handle update count of uploaded
                fs.remove(data.mediaPath);
                var sourceAction = {$setOnInsert: {type: "youtube"}};
                return Mongo.collection(Collections.ContentStudioSources).updateP({"_id": id}, sourceAction, {upsert: true});

            } else {

                rawData = processRawData(rawData, data);

                contentStudio(rawData, callback);
                return false;

            }
        }).then((result) => {

            if(result) {
                return Mongo.collection(Collections.ContentStudioWaitingList).updateP({"url":data.youtubeUrl}, {$set: {"status": StatusSplitVideoEnum.Types.SUCCEEDED}}).then(() => {

                    callback();
                    console.log(`${data.youtubeUrl} succeeded`)

                });
            }

        });

    });

};

module.exports = contentStudio;

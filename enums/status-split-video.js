var _ = require('lodash');

/**
 *
 *
 */
var StatusSplitVideoEnum = new Function();

StatusSplitVideoEnum.Types = {

    PROCESSING: "processing",
    UNPROCESSED: "unprocessed",
    SUCCEEDED: "succeeded",
    UPLOADED_BEFORE: "uploadedBefore",
    FAIL: "fail"

};

StatusSplitVideoEnum.all =_.values(StatusSplitVideoEnum.Types);

module.exports = StatusSplitVideoEnum;
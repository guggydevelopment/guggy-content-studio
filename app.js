var express = require('express');
var fs = require('fs-extra');
var path = require('path');
var winston = require('winston');
var expressWinston = require('express-winston');
var ContentStudioQueue = require('./content-studio-queue.js');

var app = express();

global.appRoot = path.resolve(__dirname);


//Access Log

app.use(
    expressWinston.logger({
        transports: [
            new winston.transports.Console({
                json: false,
                colorize: true
            })
        ],
        // optional: control whether you want to log the meta data about the request (default to true)
        meta: true,
        // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
        msg: "HTTP {{req.method}} {{req.url}}",
        // Use the default Express/morgan request formatting, with the same colors.
        // Enabling this will override any msg and colorStatus if true. Will only output colors on transports with colorize set to true
        expressFormat: true,
        // Color the status code, using the Express/morgan color palette (default green, 3XX cyan, 4XX yellow, 5XX red).
        // Will not be recognized if expressFormat is true
        colorStatus: true,
        // optional: allows to skip some log messages based on request and/or response
        ignoreRoute: function (req, res) {
            return false;
        }
    })
);


// Message body
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Routes
var router = require('./routers/router');
app.use("/", router);

fs.mkdirpSync(path.join(__dirname, 'content_studio') + '/');

// Cleanup if its production mode
if (process.env.NODE_ENV !== 'development') {

    fs.emptydirSync(path.join(__dirname, 'content_studio') + '/');

}

require('./utils/extend-objects');

/**
 *
 */
process.on('uncaughtException', function (err) {

    console.log(`${err.message} ${err.stack}`);

});

fs.emptydirSync(path.join(__dirname, 'content_studio') + '/');

let seconds = process.env.NODE_ENV !== 'development' ? 30 : 30;
const interval = 1000 * seconds;

setInterval(() => {

    var queue = ContentStudioQueue.getQueue();

    if(ContentStudioQueue.getQueue() === 0) {
        ContentStudioQueue.startProcess();
    }

}, interval);


// Create server
var http = require('http');

var server = http.createServer(app).listen(3007, function () {

    console.log("----- START DATE: " + new Date() + " -----");
    console.log("----- RUNNING IN " + process.env.NODE_ENV + " -----");
    console.log("----- EXPRESS IN " + app.get('env') + " -----");

    console.log(
        'Express server listening on port %s', 3007
    );

});


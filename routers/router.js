var express = require('express');
var router = express.Router();
var Guggy = require('../consts/consts.js').Guggy;
var version = require('../package.json').version;
var Consts = require('../consts/consts.js');
var Mongo = require('@guggy/guggy-js-utils').MongoDb.init({url: Consts.MongoDataDb.url});
var Collections = Consts.MongoDataDb.collections;
var StatusSplitVideoEnum = require('../enums/status-split-video.js');
var Promise = require('bluebird');
var Utils = require('../utils/utils.js');

function processIncomingRequest(engine,req,res){

    if (req.header('secret') && req.header('secret') === Guggy.secret) {

        engine(req.body).then(result => {

            res.json(result);

        }).catch(err => {

            console.error(err.stack);

            res.status(500).send(err.toString());

        });

    }else {

        console.error("Unauthorized access", {req: req.headers, ip: req.connection.remoteAddress});

        res.sendStatus(401);

    }

}

function checkUrlStatus(url) {

    var id;
    var mUrl = url;
    var params = Utils.searchQueryToJSON(url);

    if (params && params.v) {

        id = params.v;

    }
    var status = StatusSplitVideoEnum.Types.UNPROCESSED;

    return Mongo.collection(Collections.ContentStudio).findOneP({_id: id}).then(result =>{

        if(result) {

            status = StatusSplitVideoEnum.Types.SUCCEEDED;

        } else {

            return Mongo.collection(Collections.ContentStudioSources).findOneP({_id: id})
        }

    }).then(result => {

        if(result) {

            status = StatusSplitVideoEnum.Types.UPLOADED_BEFORE;

        }

        return Mongo.collection(Collections.ContentStudioWaitingList).updateP({url: mUrl}, {
            $setOnInsert: {
                url: mUrl,
                status: status
            }
        }, {upsert: true});

    })


}

/**
 *
 */
router.post('/contentStudio', function (req, res, next) {

    req.setTimeout(0);

    if (req.header('secret') && req.header('secret') === Guggy.secret) {

        var id, mUrl;
        var status = StatusSplitVideoEnum.Types.UNPROCESSED;
        Promise.mapSeries(req.body.urls, (url, index) => {

            return checkUrlStatus(url)

        }).finally((result) => {

            res.json("Thank you Tomer you are the best, just kidding with you, you're Baboon with hachkun eating bananas all day!");

        }).catch(err => {

            res.status(500).json(err);

        });

    } else {

        console.error("Unauthorized access", {req: req.headers, ip: req.connection.remoteAddress});

        res.sendStatus(401);

    }

});

/**
 *
 */
router.get('/version', function (req, res, next) {

    res.end(`Version: ${version}`);

});

module.exports = router;


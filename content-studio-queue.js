var async = require('async');
var contentStudioDownloadVideos = require('./bll/content-studio-download-videos.js');
var Consts = require('./consts/consts.js');
var Mongo = require('@guggy/guggy-js-utils').MongoDb.init({url: Consts.MongoDataDb.url});
var Collections = Consts.MongoDataDb.collections;
var StatusSplitVideoEnum = require('./enums/status-split-video.js');

var queue = async.queue(
    contentStudioDownloadVideos
);

var _push = queue.push;

queue.push = function (data, callback) {

    _push(data, callback);

};

var isFetchingResults = false;
queue.getQueue = function () {

    let length = isFetchingResults ? -1 :queue._tasks.length;
    return length;

};


queue.startProcess = function () {

    isFetchingResults = true;
    return Mongo.collection(Collections.ContentStudioWaitingList).findP({ $or:[{status: {$eq:StatusSplitVideoEnum.Types.PROCESSING}}, {status: {$eq:StatusSplitVideoEnum.Types.UNPROCESSED}}]}, null, null, null, {"status" : 1}).then(result => {

        result.forEach(item => {

            var obj = {url: item.url};

            if(item.status === StatusSplitVideoEnum.Types.PROCESSING) {

                obj.isInProcess = true;

            }

            if(item.status !== StatusSplitVideoEnum.Types.SUCCEEDED) {

                obj.isInProcess = true;


                if(item.lastPartUploaded) {

                    obj.lastPartUploaded = item.lastPartUploaded;

                }

                var startTime = new Date().getTime();

                _push(obj, () =>{

                    var processTime = parseFloat(new Date().getTime() - startTime);

                    Mongo.collection(Collections.ContentStudioWaitingList).updateP({url: item.url}, {$set:{processTime: processTime}});

                });
            }

        });
        isFetchingResults = false;
    }).catch(err => {

        isFetchingResults = false;
        console.error(err);

    });
};


module.exports = queue;
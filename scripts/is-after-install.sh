#!/bin/bash
cd /home/ubuntu/guggy-content-studio

# Npm install
sudo mkdir -p temp
sudo chmod 777 temp
sudo mkdir -p node_modules
sudo chmod -R 777 node_modules
echo //registry.npmjs.org/:_authToken=00353139-8d19-48a1-913e-2e160137abc9 > ~/.npmrc
npm install
npm install
#!/bin/bash
sleep 8
UPTIME=$(pm2 list -m | grep uptime | awk '{split($0,a,":") ; print a[2]}' | awk '{print substr($0, 1, length($0)-1)}')

if [ $UPTIME -ge 6 ]
then
    exit 0
else
    exit 2
fi

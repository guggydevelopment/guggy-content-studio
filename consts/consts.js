/**
 * Created by Almog Haimovitch on 25/08/2016.
 */

module.exports = {

    Guggy: function () {

        if (process.env.NODE_ENV === 'production') {

            return {
                secret: '!a!babo!be!the!chief!says!the!secrect!hut!is!over!there!'
            };

        } else {

            return {
                secret: 'yalla|hapoel|jer'
            };


        }

    }(),

    ContentStudioAWS: function () {

        if (process.env.NODE_ENV === 'production') {

            return {

                key: 'AKIAJB4IRANM4M6QFRCQ',
                secret: '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',
                bucket: 'guggy-content-studio',
                url: 'http://guggy-content-studio.s3.amazonaws.com',
                cdnUrl: 'http://guggy-content-studio.s3.amazonaws.com',
                region: 'us-east-1'
            };

        }else {

            return {

                key: 'AKIAIX546U4QLC2KGHVQ',
                secret: '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',
                bucket: 'guggyrepositorytest',
                url: 'http://guggyrepositorytest.s3.amazonaws.com',
                cdnUrl: 'http://guggyrepositorytest.s3-external-1.amazonaws.com',
                region: 'us-east-1'

            };

        }

    }(),

    MongoDataDb: function(){

        var mongo = {
            collections: {
                ContentStudio: process.env.NODE_ENV === 'production' ? "ContentStudioMedia" : "ContentStudioMediaDev",
                ContentStudioSources: process.env.NODE_ENV === 'production' ? "ContentStudioSources" : "ContentStudioSourcesDev",
                ContentStudioWaitingList: process.env.NODE_ENV === 'production' ? "ContentStudioWaitingList" : "ContentStudioWaitingListDev"
            }
        };

        if (process.env.NODE_ENV === 'development') {

            mongo.url = 'mongodb://guggy:7h1evRTzujgTkj2@ds021034.mlab.com:21034/guggy-text2gif-dev';

        }else {

            mongo.url = 'mongodb://guggy:EgMHhMe1wh@ds159988.mlab.com:59988/guggy-dashboard-data';

        }

        return mongo;

    }(),

    ContentStudio: function(){

        if (process.env.NODE_ENV === 'production') {

            return {

                FOLDER_NAME: 'content_studio',
                SUB_FOLDER_NAME: 'video',
                splitVideoWidth: 500,
                incrementSplitVideo: 3.5,
                uploadsPerBatch: 60,
                maxFailsCount: 2,
                durationSplitVideo: 5

            };

        } else {

            return {

                FOLDER_NAME: 'content_studio',
                SUB_FOLDER_NAME: 'video',
                splitVideoWidth: 500,
                incrementSplitVideo: 3.5,
                uploadsPerBatch: 60,
                maxFailsCount: 2,
                durationSplitVideo: 5
            };

        }

    }()

};
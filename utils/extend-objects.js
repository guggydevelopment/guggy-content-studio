Math.even = function (n) {
    n = Math.round(n);
    if (n % 2 !== 0) {
        n++;
    }
    return n;
};

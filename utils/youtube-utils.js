var exec = require('child_process').exec;
var Q = require('q');
var _ = require('lodash');
var fs = require('fs-extra');
var Promise = require('bluebird');


module.exports = {

    /**
     * @param destFileName
     * @param sourceFilename
     * @return {*}
     */
    download: (url, fileName, location) => {

        var cmd = 	`youtube-dl -k -f mp4 -o ${location}/${fileName}.mp4 ${url} --print-json` ;

        return Q.nfcall(exec, cmd);

    },

    splitVideos: (from, fileVideoName, location, splitIndex, fileName, width, callback) => {

        var cmd = `ffmpeg -ss ${from} -i ${fileVideoName}.mp4 -t 5 -y -vcodec libx264 -pix_fmt yuv420p -profile:v high -preset ultrafast -vf "scale=trunc(${Math.even(width)}/2)*2: trunc(ow/a/2) * 2" -crf 24 -an ${location}/${fileName}${splitIndex}.mp4`;

        return new Promise((resolve, reject) => {
            if(!fs.existsSync(`${location}/${fileName}${splitIndex}.mp4`)) {

                var startTime = new Date().getTime();

                return Q.nfcall(exec, cmd).then(result => {

                    var processTime = new Date().getTime();
                    console.log(`process time : ${processTime - startTime}`);
                    console.log(`Video split index: ${splitIndex}.mp4`);
                    resolve();


                }).catch(err => {

                    console.error(err)
                    reject(err); // todo retry

                });

            } else {

                resolve();

                console.log()

            }
        });
    }

};



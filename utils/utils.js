var EngineConsts = require('../consts/consts.js').Engine;
var path = require('path');

module.exports = {

    getReqFolder: (reqId, folderName) => {

        folderName = folderName? folderName : EngineConsts.TEMP_FOLDER;
        return path.join(global.appRoot, folderName, reqId);
    },

    searchQueryToJSON: search => {
        return search.substring(1).split("?").reduce(function (result, value) {
                var parts = value.split('=');
                if (parts[0]) result[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
                return result;
            }, {}
        )
    }

};







